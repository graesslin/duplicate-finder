/********************************************************************
Copyright (C) 2013 Martin Gräßlin <mgraesslin@kde.org>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/
#include "graph.h"
#include "bug.h"
#include "comment.h"

// QJson
#include <qjson/qobjecthelper.h>
#include <qjson/parser.h>
#include <qjson/serializer.h>
// Qt
#include <QCoreApplication>
#include <QDebug>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QProcess>
#include <QTemporaryFile>
#include <QTimer>

Node::Node(Graph *graph, qulonglong id)
    : m_graph(graph)
    , m_id(id)
{
}

Node::~Node()
{
}

void Node::setBugData(const QVariantMap &bugData)
{
    if (!m_reference.isNull()) {
        // already fetched
        return;
    }
    m_reference.reset(new Bug());
    QJson::QObjectHelper::qvariant2qobject(bugData, m_reference.data());
    const QString &dupe = m_reference->dupeOf();
    if (!dupe.isEmpty() && !hasConnection(dupe.toULongLong())) {
        m_edges << m_graph->createEdge(this, dupe, true);
    }
    m_graph->loadComments(m_id);
}

bool Node::isDuplicate() const
{
    return !m_reference->dupeOf().isEmpty();
}

bool Node::isResolved() const
{
    return m_reference->status() == "RESOLVED";
}

void Node::setComments(const QVariantList& comments)
{
    for (QVariant comment : comments) {
        Comment *c = new Comment(m_reference.data());
        QJson::QObjectHelper::qvariant2qobject(comment.toMap(), c);
        m_reference->addComment(c);
        int index = c->text().indexOf("This bug may be a duplicate of or related to");
        if (index != -1) {
            QString duplicatePart = c->text().mid(index);
            duplicatePart = duplicatePart.left(duplicatePart.indexOf('.')).mid(49);
            if (!hasConnection(duplicatePart.toULongLong())) {
                m_edges << m_graph->createEdge(this, duplicatePart, false);
            }
        }
        index = c->text().indexOf("Possible duplicates by query");
        if (index != -1) {
            QString duplicatePart = c->text().mid(index);
            duplicatePart = duplicatePart.left(duplicatePart.indexOf('.')).mid(30);
            duplicatePart = duplicatePart.replace("bug ", "");
            const QStringList bugs = duplicatePart.split(", ");
            for (const QString &id : bugs) {
                if (!hasConnection(id.toULongLong())) {
                    m_edges << m_graph->createEdge(this, id, false);
                }
            }
        }
    }
}

bool Node::hasConnection(qulonglong bugId) const
{
    for (Edge *edge : m_edges) {
        if (edge->end()->id() == bugId) {
            return true;
        }
    }
    return false;
}

Node* Node::duplicateOf()
{
    qulonglong id = m_reference->dupeOf().toLongLong();
    for (Edge *edge : m_edges) {
        if (edge->end()->id() == id) {
            if (edge->end()->isDuplicate()) {
                return edge->end()->duplicateOf();
            } else {
                return edge->end();
            }
        }
    }
    return nullptr;
}

Edge::Edge(Node* start, Node* end, bool duplicate)
    : m_start(start)
    , m_end(end)
    , m_duplicate(duplicate)
{
}

Edge::~Edge()
{
}

Graph::Graph(const QString &startId)
    : QObject(nullptr)
    , m_manager(new QNetworkAccessManager(this))
{
    connect(m_manager, SIGNAL(finished(QNetworkReply*)), SLOT(loadFinished(QNetworkReply*)));
    connect(this, SIGNAL(nodeAdded(Node*)), SLOT(scheduleFetch(Node*)));
    getNode(startId);
}

Graph::~Graph()
{
    qDeleteAll(m_edges);
    qDeleteAll(m_nodes);
}

Node *Graph::getNode(const QString &bugId)
{
    const qulonglong id = bugId.toULongLong();
    for (Node *node : m_nodes) {
        if (node->id() == id) {
            return node;
        }
    }
    Node *node = new Node(this, id);
    m_nodes << node;
    emit nodeAdded(node);
    return node;
}

Edge* Graph::createEdge(Node* start, const QString& targetId, bool duplicate)
{
    Edge *edge = new Edge(start, getNode(targetId), duplicate);
    m_edges << edge;

    emit edgeAdded(edge);
    return edge;
}

void Graph::scheduleFetch(Node *node)
{
    m_bugsToFetch << node->id();
    m_bugsBeingFetched << node->id();
    QTimer::singleShot(0, this, SLOT(startLoading()));
}

void Graph::loadFinished(QNetworkReply* reply)
{
    QVariant type = reply->property("requestType");
    if (!type.isValid()) {
        qDebug() << "requestType for network request not found";
        qApp->quit();
        return;
    }
    if (type.toString() == "bugs") {
        bugsLoaded(reply);
    } else if (type.toString() == "comments") {
        commentsLoaded(reply);
    } else {
        qDebug() << "Unknown requestType for network request found";
        qApp->quit();
    }
    if (m_bugsBeingFetched.isEmpty() && m_commentsBeingFetched.isEmpty()) {
        findMostLikelyDuplicate();
        // TODO: move into thread
        generatePng();
    }
    reply->deleteLater();
}

void Graph::startLoading()
{
    if (m_bugsToFetch.isEmpty()) {
        return;
    }
    QUrl bugtracker("https://bugs.kde.org");
    bugtracker.setPath("jsonrpc.cgi");
    QVariantMap args;
    QVariantList bugs;
    args.insert("ids", m_bugsToFetch);
    m_bugsToFetch.clear();
    QJson::Serializer serializer;
    QByteArray serializedData = serializer.serialize(QVariantList() << args);
    bugtracker.setQueryItems(QList<QPair<QString, QString>>() << QPair<QString, QString>("method", "Bug.get") << QPair<QString, QString>("params", serializedData));
    QNetworkReply *reply = m_manager->get(QNetworkRequest(bugtracker));
    reply->setProperty("requestType", "bugs");
}

void Graph::bugsLoaded(QNetworkReply* reply)
{
    bool ok = false;
    QJson::Parser parser;
    QVariantMap parsedData = parser.parse(reply, &ok).toMap();
    if (!ok) {
        qDebug() << "Parsing of JSON result failed";
        qApp->quit();
        return;
    }
    if (!parsedData["error"].isNull()) {
        QVariantMap error = parsedData["error"].toMap();
        qDebug() << "Error while loading Bug list: " << error["message"].toString() << "(" << error["code"].toString() << ")";
        qApp->quit();
        return;
    }
    QVariantList bugs = parsedData["result"].toMap().value("bugs").toList();
    for (const auto &bug : bugs) {
        const QVariantMap bugData = bug.toMap();
        const qulonglong id = bugData.value("id").toULongLong();
        for (Node *node : m_nodes) {
            if (node->id() == id) {
                m_bugsBeingFetched.removeAll(node->id());
                node->setBugData(bugData);
                break;
            }
        }
    }
}

void Graph::loadComments(qulonglong id)
{
    m_commentsToFetch << id;
    m_commentsBeingFetched << id;
    QTimer::singleShot(0, this, SLOT(startCommentLoading()));
}

void Graph::startCommentLoading()
{
    if (m_commentsToFetch.isEmpty()) {
        return;
    }
    QUrl bugtracker("https://bugs.kde.org");
    bugtracker.setPath("jsonrpc.cgi");
    QVariantMap args;
    args.insert("ids", m_commentsToFetch);
    m_commentsToFetch.clear();
    QJson::Serializer serializer;
    QByteArray serializedData = serializer.serialize(QVariantList() << args);
    bugtracker.setQueryItems(QList<QPair<QString, QString>>() << QPair<QString, QString>("method", "Bug.comments") << QPair<QString, QString>("params", serializedData));
    QNetworkReply *reply = m_manager->get(QNetworkRequest(bugtracker));
    reply->setProperty("requestType", "comments");
}

void Graph::commentsLoaded(QNetworkReply* reply)
{
    bool ok = false;
    QJson::Parser parser;
    QVariantMap parsedData = parser.parse(reply, &ok).toMap();
    if (!ok) {
        qDebug() << "Parsing of JSON result failed";
        qApp->quit();
        return;
    }
    if (!parsedData["error"].isNull()) {
        QVariantMap error = parsedData["error"].toMap();
        qDebug() << "Error while loading Bug list: " << error["message"].toString() << "(" << error["code"].toString() << ")";
        qApp->quit();
        return;
    }
    QVariantMap bugs = parsedData["result"].toMap().value("bugs").toMap();
    for (auto it = bugs.constBegin(); it != bugs.constEnd(); ++it) {
        const qulonglong id = it.key().toULongLong();
        for (Node *node : m_nodes) {
            if (node->id() == id) {
                m_commentsBeingFetched.removeAll(node->id());
                node->setComments(it.value().toMap().value("comments").toList());
                emit commentsFetched(node);
                break;
            }
        }
    }
}

void Graph::generatePng()
{
    QTemporaryFile file;
    if (!file.open()) {
        return;
    }
    file.write(generateDot().toUtf8());
    file.flush();
    QStringList arguments;
    arguments << "-Tpng";
    arguments << "-O";
    arguments << file.fileName();
    QProcess::execute("dot", arguments);
    emit imageCreated(file.fileName() + ".png");
}

QString Graph::generateDot() const
{
    QString dot("digraph graphname {\n");
    for (Node *node : m_nodes) {
        dot.append(QString::number(node->id()));
        if (node->isDuplicate()) {
            dot.append(" [color=red]");
        } else if (node->isResolved()) {
            dot.append(" [color=red]");
        }
        dot.append("\n");
    }
    for (Edge *edge : m_edges) {
        dot.append(QString::number(edge->start()->id()));
        dot.append(" -> ");
        dot.append(QString::number(edge->end()->id()));
        if (edge->isDuplicate()) {
            dot.append(" [color=red]");
        }
        dot.append(";\n");
    }
    dot.append("}");
    return dot;
}

void Graph::findMostLikelyDuplicate()
{
    QHash<Node*, int> scores;
    for (Node *node : m_nodes) {
        scores.insert(node, 0);
    }
    for (Edge *edge : m_edges) {
        scores[edge->end()]++;
    }
    int maxScore = 0;
    QList<Node*> duplicates;
    for (auto it = scores.constBegin(); it != scores.constEnd(); ++it) {
        if (it.value() < maxScore) {
            continue;
        } else if (it.value() == maxScore) {
            duplicates.append(it.key());
            continue;
        }
        duplicates.clear();
        duplicates.append(it.key());
        maxScore = it.value();
    }
    // check that our most likely dupes are not duplicates themselves
    QList<Node*> toRemove;
    QList<Node*> toInsert;
    for (Node *node : duplicates) {
        if (node->isDuplicate()) {
            toRemove.append(node);
            if (Node *dupe = node->duplicateOf()) {
                if (!toInsert.contains(dupe) && !duplicates.contains(dupe)) {
                    toInsert << dupe;
                }
            }
        }
    }
    for (Node *node : toRemove) {
        duplicates.removeAll(node);
    }
    duplicates.append(toInsert);
    emit mostLikelyDuplicatesFound(duplicates);
}

QList< Node* > Graph::notDuplicateBugs() const
{
    QList<Node*> nodes;
    for (Node *node : m_nodes) {
        if (!node->isDuplicate()) {
            nodes << node;
        }
    }
    return nodes;
}

#include "graph.moc"
