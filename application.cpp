/********************************************************************
Copyright (C) 2013 Martin Gräßlin <mgraesslin@kde.org>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/
#include "application.h"
#include "bug.h"
#include "comment.h"
#include "graph.h"

// KDE
#include <KDE/Mailtransport/MessageQueueJob>
#include <KDE/Mailtransport/TransportManager>
#include <KDE/KDialog>

#include <QAction>
#include <QBoxLayout>
#include <QDialog>
#include <QLineEdit>
#include <QMainWindow>
#include <QDebug>
#include <QLabel>
#include <QScrollArea>
#include <QSplitter>
#include <QTextEdit>
#include <QToolBar>

Application::Application(QObject* parent)
    : QObject(parent)
    , m_window(new QMainWindow())
    , m_view(new MainView())
    , m_duplicateAction(new QAction("Mark as Duplicate", this))
    , m_nextBugAction(new QAction("Next Bug", this))
    , m_newBugAction(new QAction("New Run", this))
    , m_selectedDuplicate(nullptr)
{
}

Application::~Application()
{
}

void Application::start()
{
    if (!m_graph.isNull()) {
        return;
    }
    // add actions to Toolbar
    QToolBar *toolBar = m_window->addToolBar("Main");
    m_newBugAction->setShortcut(QKeySequence("Ctrl+N"));
    m_duplicateAction->setShortcut(QKeySequence("Ctrl+D"));
    m_duplicateAction->setEnabled(false);
    m_nextBugAction->setEnabled(false);

    toolBar->addAction(m_newBugAction);
    toolBar->addAction(m_nextBugAction);
    toolBar->addAction(m_duplicateAction);
    connect(m_newBugAction, SIGNAL(triggered(bool)), SLOT(newRun()));
    connect(m_duplicateAction, SIGNAL(triggered(bool)), SLOT(markAsDuplicate()));
    connect(m_nextBugAction, SIGNAL(triggered(bool)), SLOT(nextBug()));
    // create the window
    m_window->setCentralWidget(m_view);
    m_window->show();
    createGraph();
}

void Application::createGraph()
{
    m_graph.reset(new Graph(m_startId));
    connect(m_graph.data(), SIGNAL(commentsFetched(Node*)), SLOT(commentsFetched(Node*)));
    connect(m_graph.data(), SIGNAL(imageCreated(QString)), SLOT(imageCreated(QString)));
    connect(m_graph.data(), SIGNAL(mostLikelyDuplicatesFound(QList<Node*>)), SLOT(mostLikelyDuplicatesFound(QList<Node*>)));
}

void Application::commentsFetched(Node* node)
{
    if (QString::number(node->id()) == m_startId) {
        m_view->setStartBug(nodeToText(node));
    }
}

QString Application::nodeToText(Node* node) const
{
    QString text;
    text.append(QString::number(node->id()));
    text.append("\n\n");
    for (Comment *c : node->reference()->comments()) {
        text.append(c->text());
        text.append("\n\n");
        text.append("-------------------------------------\n\n");
    }
    return text;
}

void Application::imageCreated(const QString& fileName)
{
    QImage image(fileName);
    m_view->setImage(QPixmap::fromImage(image));
    // TODO: can we delete the file here?
}

void Application::mostLikelyDuplicatesFound(const QList<Node *> duplicates)
{
    if (duplicates.isEmpty()) {
        // TODO: show error message
        m_duplicateAction->setEnabled(false);
        m_nextBugAction->setEnabled(false);
        return;
    }
    m_selectedDuplicate = duplicates.first();
    m_view->setDuplicateBug(nodeToText(duplicates.first()));
    m_duplicateAction->setEnabled(true);
    m_nextBugAction->setEnabled(true);
}

void Application::markAsDuplicate()
{
    KMime::Message *m = new KMime::Message;
    m->contentTransferEncoding()->clear();
    m->from()->fromUnicodeString(m_sender, "utf-8");
    m->to()->fromUnicodeString(m_bugzillaEmail, "utf-8");
    m->date()->setDateTime(KDateTime::currentLocalDateTime());
    m->subject()->fromUnicodeString("[Bug " + QString::number(m_currentBugId) + "]", "utf-8");
    m->setBody(QString("@dupe_of " + QString::number(m_selectedDuplicate->id()) + "\n").toUtf8());
    m->assemble();
    // job
    MailTransport::MessageQueueJob *job = new MailTransport::MessageQueueJob(this);
    job->setMessage(KMime::Message::Ptr(m));
    job->transportAttribute().setTransportId(MailTransport::TransportManager::self()->defaultTransportId());
    job->addressAttribute().setFrom(m_sender);
    job->addressAttribute().setTo(QStringList() << m_bugzillaEmail);
    job->start();
}

void Application::nextBug()
{
    QList<Node*> nonDuplicates = m_graph->notDuplicateBugs();
    if (nonDuplicates.isEmpty()) {
        m_nextBugAction->setEnabled(false);
    }
    auto it = nonDuplicates.constBegin();
    for (; it != nonDuplicates.constEnd(); ++it) {
        if ((*it)->id() == m_currentBugId) {
            ++it;
            break;
        }
    }
    if (it == nonDuplicates.constEnd()) {
        it = nonDuplicates.constBegin();
    }
    if (m_currentBugId != (*it)->id()) {
        m_currentBugId = (*it)->id();
        m_view->setStartBug(nodeToText(*it));
    } else {
        m_nextBugAction->setEnabled(false);
    }
}

void Application::newRun()
{
    KDialog *dialog = new KDialog();
    QLineEdit *edit = new QLineEdit(dialog);
    dialog->setMainWidget(edit);
    if (dialog->exec() == QDialog::Accepted) {
        m_startId = edit->text();
        m_currentBugId = m_startId.toULongLong();
        m_selectedDuplicate = nullptr;
        m_duplicateAction->setEnabled(false);
        m_nextBugAction->setEnabled(false);
        m_view->setDuplicateBug("");
        m_view->setStartBug("");
        createGraph();
    }
    delete dialog;
}

MainView::MainView(QWidget* parent, Qt::WindowFlags f)
    : QWidget(parent, f)
    , m_startBug(new QTextEdit(this))
    , m_duplicateBug(new QTextEdit(this))
    , m_scrollArea(new QScrollArea(this))
{
    setup();
}

MainView::~MainView()
{
}

void MainView::setup()
{
    m_startBug->setReadOnly(true);
    m_duplicateBug->setReadOnly(true);
    QSplitter *splitter = new QSplitter(this);
    splitter->setOrientation(Qt::Vertical);

    QSplitter *horizontalSplitter = new QSplitter(this);
    horizontalSplitter->addWidget(m_startBug);
    horizontalSplitter->addWidget(m_duplicateBug);

    splitter->addWidget(m_scrollArea);
    splitter->addWidget(horizontalSplitter);

    QVBoxLayout *layout = new QVBoxLayout(this);
    layout->addWidget(splitter);
    setLayout(layout);
}

void MainView::setStartBug(const QString& text)
{
    m_startBug->setText(text);
}

void MainView::setDuplicateBug(const QString &text)
{
    m_duplicateBug->setText(text);
}

void MainView::setImage(const QPixmap& image)
{
    QLabel *imageLabel = new QLabel(this);
    imageLabel->setPixmap(image);
    m_scrollArea->setWidget(imageLabel);
    imageLabel->show();
}

#include "application.moc"
