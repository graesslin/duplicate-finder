/********************************************************************
Copyright (C) 2013 Martin Gräßlin <mgraesslin@kde.org>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/
#ifndef APPLICATION_H
#define APPLICATION_H
#include <QObject>
#include <QWidget>

class Graph;
class MainView;
class Node;
class QAction;
class QMainWindow;
class QScrollArea;
class QTextEdit;

class Application : public QObject
{
    Q_OBJECT
public:
    explicit Application(QObject* parent = 0);
    virtual ~Application();

    void setBugzillaUrl(const QString &bugzillaUrl);
    void setStartId(const QString &startId);
    void setBugzillaEmail(const QString &address);
    void setSender(const QString &sender);
    void start();
private Q_SLOTS:
    void commentsFetched(Node *node);
    void imageCreated(const QString &fileName);
    void mostLikelyDuplicatesFound(const QList<Node *> duplicates);
    void markAsDuplicate();
    void nextBug();
    void newRun();
private:
    void createGraph();
    QString nodeToText(Node *node) const;
    QString m_bugzillaUrl;
    QString m_startId;
    qulonglong m_currentBugId;
    QString m_bugzillaEmail;
    QString m_sender;
    QScopedPointer<Graph> m_graph;
    QScopedPointer<QMainWindow> m_window;
    MainView *m_view;
    QAction *m_duplicateAction;
    QAction *m_nextBugAction;
    QAction *m_newBugAction;
    Node *m_selectedDuplicate;
};

class MainView : public QWidget
{
    Q_OBJECT
public:
    explicit MainView(QWidget* parent = 0, Qt::WindowFlags f = 0);
    virtual ~MainView();

    void setStartBug(const QString &text);
    void setDuplicateBug(const QString &text);
    void setImage(const QPixmap &image);
private:
    void setup();
    QTextEdit *m_startBug;
    QTextEdit *m_duplicateBug;
    QScrollArea *m_scrollArea;
};

inline
void Application::setBugzillaUrl(const QString &bugzillaUrl)
{
    m_bugzillaUrl = bugzillaUrl;
}

inline
void Application::setStartId(const QString &startId)
{
    m_startId = startId;
    m_currentBugId = m_startId.toULongLong();
}

inline
void Application::setBugzillaEmail(const QString &address)
{
    m_bugzillaEmail = address;
}

inline
void Application::setSender(const QString &sender)
{
    m_sender = sender;
}

#endif //  APPLICATION_H
