/********************************************************************
Copyright (C) 2013 Martin Gräßlin <mgraesslin@kde.org>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/
#include <QApplication>
#include <QStringList>

#include "application.h"

#include <iomanip>
#include <iostream>

static const QString KDE_BUGZILLA_URL = "https://bugs.kde.org";
static const QString KDE_BUGZILLA_MAIL = "bug-control@bugs.kde.org";

void printHelp()
{
    auto printOption = [](const char *flag, const char *description) {
        std::cout << std::setw(30) << std::left << flag << description << std::endl;
    };
    std::cout << "Duplicate Bug finder" << std::endl << std::endl;
    std::cout << "Usage: duplicatefinder [options] bugId" << std::endl << std::endl;

    printOption("-h, --help",                   "Shows this help");
    printOption("--url=<url>",                  "Bugzilla url, if not specified https://bugs.kde.org is used");
    printOption("--to=<email>",                 "Bugzilla email interface, if not specified bug-control@bugs.kde.org");
    printOption("--from=<email>",               "From address to send duplicate mails");
}

int main(int argc, char** argv)
{
    QApplication app(argc, argv);
    QString bugzillaUrl, bugzillaTo, from;
    auto testArgument = [&](QString &variable, const QString &argumentStart, const QString &defaultValue = QString()) {
        for (const QString &argument : app.arguments()) {
            if (argument.startsWith(argumentStart)) {
                variable = argument.right(argument.length() - argumentStart.length());
                break;
            }
        }
        if (variable.isNull()) {
            variable = defaultValue;
        }
    };
    testArgument(bugzillaUrl, "--url=", KDE_BUGZILLA_URL);
    testArgument(bugzillaTo, "--to=", KDE_BUGZILLA_MAIL);
    testArgument(from, "--from=", "");
    for (const QString &argument : app.arguments()) {
        if (argument == "--help" || argument == "-h") {
            printHelp();
            return 0;
        }
    }
    Application application;
    application.setBugzillaUrl(bugzillaUrl);
    application.setBugzillaEmail(bugzillaTo);
    application.setSender(from);
    application.setStartId(app.arguments().last());
    application.start();
    return app.exec();
}
