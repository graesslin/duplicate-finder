/********************************************************************
Copyright (C) 2013 Martin Gräßlin <mgraesslin@kde.org>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/
#ifndef GRAPH_H
#define GRAPH_H

#include <QObject>
#include <QList>
#include <QScopedPointer>
#include <QString>
#include <QVariantMap>

// forward declarations
class Bug;
class Edge;
class Graph;
class QNetworkReply;
class QNetworkAccessManager;

class Node {
public:
    Node(Graph *parent, qulonglong id);
    virtual ~Node();

    qulonglong id() const;
    void setBugData(const QVariantMap &map);
    void setComments(const QVariantList &comments);
    bool hasConnection(qulonglong bugId) const;
    bool isDuplicate() const;
    bool isResolved() const;
    Node *duplicateOf();
    const Bug *reference() const;
private:
    Graph *m_graph;
    qulonglong m_id;
    QScopedPointer<Bug> m_reference;
    QList<Edge*> m_edges;
};

class Edge {
public:
    Edge(Node *start, Node *end, bool duplicate = false);
    virtual ~Edge();

    bool isDuplicate() const;
    const Node *start() const;
    const Node *end() const;
    Node *end();
private:
    Node *m_start;
    Node *m_end;
    bool m_duplicate;
};

class Graph : public QObject {
    Q_OBJECT
public:
    Graph(const QString &startId);
    virtual ~Graph();
    Edge *createEdge(Node *start, const QString &targetId, bool duplicate);
    void loadComments(qulonglong id);

    QList<Node*> notDuplicateBugs() const;

Q_SIGNALS:
    void edgeAdded(Edge *edge);
    void nodeAdded(Node *node);
    void commentsFetched(Node *node);
    void imageCreated(const QString &filename);
    void mostLikelyDuplicatesFound(const QList<Node *> duplicates);
private Q_SLOTS:
    void startLoading();
    void startCommentLoading();
    void loadFinished(QNetworkReply *reply);
    void scheduleFetch(Node *node);
private:
    Node *getNode(const QString &bugId);
    void bugsLoaded(QNetworkReply *reply);
    void commentsLoaded(QNetworkReply *reply);
    QString generateDot() const;
    void generatePng();
    void findMostLikelyDuplicate();
    QList<Node*> m_nodes;
    QList<Edge*> m_edges;
    QVariantList m_bugsToFetch;
    QVariantList m_commentsToFetch;
    QList<qulonglong> m_bugsBeingFetched;
    QList<qulonglong> m_commentsBeingFetched;
    QNetworkAccessManager *m_manager;
};

inline
qulonglong Node::id() const
{
    return m_id;
}

inline
const Bug *Node::reference() const
{
    return m_reference.data();
}

inline
bool Edge::isDuplicate() const
{
    return m_duplicate;
}

inline
const Node* Edge::end() const
{
    return m_end;
}

inline
Node* Edge::end()
{
    return m_end;
}

inline
const Node* Edge::start() const
{
    return m_start;
}

#endif
